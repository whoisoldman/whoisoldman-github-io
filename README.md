<!--
**whoisoldman/whoisoldman.github.io** is a ✨ _special_ ✨ repository because its `README.md` (this file) appears on your GitHub profile.

Here are some ideas to get you started:

- 🔭 I’m currently working on ...
- 🌱 I’m currently learning ...
- 👯 I’m looking to collaborate on ...
- 🤔 I’m looking for help with ...
- 💬 Ask me about ...
- 📫 How to reach me: ...
- 😄 Pronouns: ...
- ⚡ Fun fact: ...
-->

# Hi, i'm Mr.OLDMAN 👋
I have excellent experience (over 25 years) in marketing and enterprise development management. He went from a simple manager to Director of Development and Marketing of a large international investment holding company. In general, he worked on creating a high-margin business with a vertically integrated management structure. But this is in a previous life ...

### Now my status is: student of the Python course-developer (Level: &#x1f535; JUNIOR).
### My statistics:
<p align='center'>
   <a href="https://github-readme-stats.vercel.app/api?username=whoisoldman&show_icons=true&count_private=true"><img
      height=150
      src="https://github-readme-stats.vercel.app/api?username=whoisoldman&show_icons=true&count_private=true"/></a>
   <a href="https://github.com/romankh3/github-readme-stats"><img height=150
   src="https://github-readme-stats.vercel.app/api/top-langs/?username=whoisoldman&layout=compact"/></a>
</p>

### Key points
*   owner of [SoundBloqs Community](https://soundbloqs.com/) <!-- and [Template Repository](https://github.com/template-repository) organizations.-->
*   owner of [TrustMoney.UK](https://trustmoney.uk/) - online Project manager of global private consulting Club (world transfers, trading, M0-M1 funds, VISA.NET, investing solution, energy, gold, food, blockchain and cryptocurrencies and more ...)
*   Write posts about software development, music, art in its general sense and, of course, about talented people who make our world beautiful!
*   Currently study in [ProductStar](https://productstar.ru/)

## 🛠 Technical Stack
*   Python language (Level: Junior)
*   SQL (SQLite, PostgreSQL)
*   Git/GitHub/GitLab/GitLab CI
*   API on Flask

### My opensource projects

*   GitHub [analyze_clipboard_text](https://github.com/whoisoldman/analyze_clipboard_text.git)
*   Coming soon more ...

<p align='center'>
   <a href="https://www.linkedin.com/in/soundbloqs/">
      💬 <img src="https://img.shields.io/badge/linkedin-%230077B5.svg?&style=for-the-badge&logo=linkedin&logoColor=white"/> 💬
   </a>
   <a href="https://t.me/soundbloqsfb_bot">
       <img src="https://img.shields.io/badge/Telegram-2CA5E0?style=for-the-badge&logo=telegram&logoColor=white"/> 💬
   </a>
<p align='center'>
   How to reach me: <a href='mailto:it@dontnsp.am'>Mr.OLDMAN's Email</a>
</p>
<p align="center">
   ... just stay Humane!
</p>
